# Useful links
- Web wallet: https://www.myetherwallet.com/
- Web solidity code editor: https://remix.ethereum.org/
- Bitcoin chain explorer: https://www.blockchain.com/explorer
- Ethereum chain explorer: https://etherscan.io/
- Bitcoin white paper: https://bitcoin.org/bitcoin.pdf
- Ethereum yellow paper: https://ethereum.github.io/yellowpaper/paper.pdf
- Blockchain basics playground: https://andersbrownworth.com/blockchain/hash