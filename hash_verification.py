import hashlib
import binascii

# 1. Getting header from https://blockstream.info
# Current parameters are valid for block 677292 with hash 000000000000000000035d4fcaaddbe0027479d0dedbb66dcd0f3c597fa5193a
version = "20400000" # 1
hashPrevBlock = "0000000000000000000212c712e5c99311f690e3e7dfd1107be69f93dfa53a7d"
hashMerkleRoot = "0b405086c28924f6631d5b090b680ad175c4f0c87cdbe9a2c667a8ec63181475"
time = "6065A0B9" # Epoch time as hex
bits = "170cdf6f" # Hex
nonce = 1958621044 # in decimal notation
nonce = hex(int(0x100000000)+nonce)[-8:] # in 4-byte hex notation "9546a142"

# 2. Convert them in little-endian hex notation
version = binascii.hexlify(binascii.unhexlify(version)[::-1])
hashPrevBlock = binascii.hexlify(binascii.unhexlify(hashPrevBlock)[::-1])
hashMerkleRoot = binascii.hexlify(binascii.unhexlify(hashMerkleRoot)[::-1])
time = binascii.hexlify(binascii.unhexlify(time)[::-1])
bits = binascii.hexlify(binascii.unhexlify(bits)[::-1])
nonce = binascii.hexlify(binascii.unhexlify(nonce)[::-1])

# 3. Concatenating header values
header = version+hashPrevBlock+hashMerkleRoot+time+bits+nonce

# 4. Taking the double-SHA256 hash value
header = binascii.unhexlify(header)
hash = hashlib.sha256(hashlib.sha256(header).digest()).digest()
hash = binascii.hexlify(hash)

# 5. Converting the hash value in big-endian hex notation
hash = binascii.hexlify(binascii.unhexlify(hash)[::-1])
print(hash)